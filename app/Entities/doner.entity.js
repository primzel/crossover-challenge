"use strict";
var Doner = (function () {
    function Doner(first_name, last_name, contact_number, email, address, bloodGroup) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.contact_number = contact_number;
        this.email = email;
        this.address = address;
        this.bloodGroup = bloodGroup;
    }
    return Doner;
}());
exports.Doner = Doner;
