import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {AppComponent}  from './components/AppComponent';
import {LoginPage} from "./components/LoginPage";
import {MapComponent} from "./components/map.component";
import {FormModal} from "./components/form.modal.component";
import { HttpModule, JsonpModule } from '@angular/http';
import {DonnerFormComponent} from "./components/form.doner.component";
@NgModule({
    imports: [BrowserModule, FormsModule,ReactiveFormsModule,HttpModule],
    declarations: [MapComponent,FormModal,DonnerFormComponent],
    bootstrap: [MapComponent,FormModal,DonnerFormComponent]
})
export class AppModule {
}