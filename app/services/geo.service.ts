import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
export default class GeoService {
    constructor (@Injectable(Http) private http: Http) {}
    locationUrl="http://ipinfo.io/json";
    public getPosition = () => {
        return this.http.get(this.locationUrl).map(res => res.json()).catch(this.handleError);;
    }

    private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}