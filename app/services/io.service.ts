/// <reference path="../../typings.d.ts" />
import * as io from 'socket.io-client';

export default class IOService{
    socket:any;
    constructor(){
        this.socket=io();
    }
    public emit=(event:string,data?:any)=>{
        this.socket.emit(event,data);
    }
    public on=(event:string,callback:(data:any)=>any):void=>{
        this.socket.on(event,callback);
    }
}