"use strict";
/// <reference path="../../typings.d.ts" />
var io = require('socket.io-client');
var IOService = (function () {
    function IOService() {
        var _this = this;
        this.emit = function (event, data) {
            _this.socket.emit(event, data);
        };
        this.on = function (event, callback) {
            _this.socket.on(event, callback);
        };
        this.socket = io();
    }
    return IOService;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = IOService;
