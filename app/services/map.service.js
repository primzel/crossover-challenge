"use strict";
var Map_1 = require('esri/Map');
var MapService = (function () {
    function MapService() {
        this.name = 1;
        this.map = new Map_1.default({ basemap: 'topo' });
    }
    return MapService;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MapService;
