import {Component, ElementRef, Output, EventEmitter, ViewChild} from '@angular/core';
import MapService from '../services/map.service';
import GeoService from "../services/geo.service";
import MapView from "esri/views/MapView";
import Graphic from 'esri/Graphic';
import PopupTemplate from 'esri/PopupTemplate';
import Point from 'esri/geometry/Point';
import PictureMarkerSymbol from 'esri/symbols/PictureMarkerSymbol';
import SimpleMarkerSymbol from 'esri/symbols/SimpleMarkerSymbol';
import FormModal from "../components/form.modal.component";
import IOService from "../services/io.service";
import GraphicsLayer from "esri/layers/GraphicsLayer"
@Component({
    selector: 'esri-map',
    template: '<div id="viewDiv"><ng-content></ng-content></div><form-modal #donerModal></form-modal>',
    providers: [MapService, GeoService, IOService],

})
export class MapComponent {

    @Output() viewCreated = new EventEmitter();
    @ViewChild("donerModal") formModal:FormModal;
    view:any;

    constructor(private _service:MapService, private _geoService:GeoService, private elRef:ElementRef, private _ioService:IOService) {
    }

    ngOnInit() {

        this.view = new MapView({
            container: this.elRef.nativeElement.firstChild,
            map: this._service.map,
            zoom: 10,
            center: [31.5546, 74.3572]
        });

        this._ioService.on("addPin", this.addpin);

        this.view.on("click", (evt) => {
            if (evt.target.localName == "circle") {
                console.log(evt.target.localName);


            } else {
                this.formModal.open(evt.mapPoint);
            }
            //this.view.graphics.removeAll();

// I added this line to make sure the previous pin is removed if the new
//feature is clicked. If there is a better way to handle it, let me know.

        });
        this.getCoordinates();
        this.viewCreated.next(this.view);
    }

    private addpin = (data)=> {

        // Create a symbol for drawing the point
        var markerSymbol = new SimpleMarkerSymbol({
            color: [226, 119, 40],
            outline: { // autocasts as new SimpleLineSymbol()
                color: [255, 255, 255],
                width: 2
            }
        });
        console.log(data);

        var point = new Point({
            x: data.x,
            y: data.y,
            spatialReference: 2027
        });
        // Create a graphic and add the geometry and symbol to it
        var pointInfoTemplate = new PopupTemplate({
            title: 'Name : {name}',
            content: [{
                type: "media",
                mediaInfos: [{
                    title: "<b>{address},{contactNumber}</b>",
                    type: "image",
                    value: {
                        sourceURL: "/images/blood.png" // use an attribute dynamically in the URL
                    }
                }]
            }]
        });
        var pointGraphic = new Graphic(point, markerSymbol, data, pointInfoTemplate);
        this.view.graphics.add(pointGraphic);
    }

    getCoordinates() {
        var cords:any;
        this._geoService.getPosition().subscribe(val => {
            val = val.loc.split(",");
            console.log(val);
            this.view.center = new Point(val[1], val[0]);
        });

    }
}
