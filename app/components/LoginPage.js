"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by qasim on 9/1/16.
 */
var core_1 = require("@angular/core");
// import {NgClass} from '@angular/common';
var forms_1 = require('@angular/forms');
var forms_2 = require('@angular/forms');
;
var common_1 = require('@angular/common');
var LoginPage = (function () {
    function LoginPage(fb) {
        this.loginForm = fb.group({
            email: ["", common_1.Validators.required],
            password: ["", common_1.Validators.required],
        });
    }
    LoginPage.prototype.doLogin = function (event) {
        console.log(this.loginForm.value);
        event.preventDefault();
    };
    LoginPage = __decorate([
        core_1.Component({
            selector: 'login-page',
            templateUrl: './templates/login-form.html',
            directives: [common_1.CORE_DIRECTIVES, forms_1.FORM_DIRECTIVES, forms_1.REACTIVE_FORM_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [forms_2.FormBuilder])
    ], LoginPage);
    return LoginPage;
}());
exports.LoginPage = LoginPage;
