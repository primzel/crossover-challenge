/**
 * Created by qasimkhokhar on 9/5/16.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var forms_1 = require('@angular/forms');
var forms_2 = require('@angular/forms');
var common_1 = require('@angular/common');
var core_1 = require("@angular/core");
var io_service_1 = require("../services/io.service");
var DonnerFormComponent = (function () {
    function DonnerFormComponent(fb, _ioService) {
        this._ioService = _ioService;
        this.donnerForm = new forms_2.FormGroup({
            first_name: new forms_2.FormControl("", common_1.Validators.required),
            last_name: new forms_2.FormControl("", common_1.Validators.required),
            email: new forms_2.FormControl("", common_1.Validators.required),
            contact_number: new forms_2.FormControl("", common_1.Validators.required),
            bloodGroup: new forms_2.FormControl("", common_1.Validators.required),
            address: new forms_2.FormControl("", common_1.Validators.required),
        });
    }
    DonnerFormComponent.prototype.saveDonner = function (event) {
        console.log(this.donnerForm.value, this.dataobj);
        var payLoad = this.donnerForm.value;
        payLoad.x = this.dataobj.x;
        payLoad.y = this.dataobj.y;
        payLoad.mapPoint = this.dataobj;
        this._ioService.emit("saveDonner", this.donnerForm.value);
        event.preventDefault();
        return false;
    };
    DonnerFormComponent = __decorate([
        core_1.Component({
            selector: "donner-form",
            templateUrl: "../templates/donner.form.template.html",
            directives: [common_1.CORE_DIRECTIVES, forms_1.FORM_DIRECTIVES, forms_1.REACTIVE_FORM_DIRECTIVES],
            providers: [io_service_1.default],
            inputs: ["dataobj"]
        }), 
        __metadata('design:paramtypes', [forms_2.FormBuilder, io_service_1.default])
    ], DonnerFormComponent);
    return DonnerFormComponent;
}());
exports.DonnerFormComponent = DonnerFormComponent;
