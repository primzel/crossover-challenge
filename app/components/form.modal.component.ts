import {Component, ElementRef, Output, EventEmitter, ViewChild} from '@angular/core';
import {MODAL_DIRECTIVES, ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';
import {CORE_DIRECTIVES, Validators} from '@angular/common';
import DonnerFormComponent from "../components/form.doner.component";
@Component({
    selector: "form-modal",
    templateUrl: "../templates/form.modal.template.html",
    directives: [CORE_DIRECTIVES, MODAL_DIRECTIVES]
})

export class FormModal {
    @Output() viewCreated = new EventEmitter();
    @ViewChild("myModal") modal:ModalComponent;
    public mapPoints:any;

    close() {
        this.modal.close();
    }

    open(points:any) {
        console.log(points);
        this.mapPoints = points;
        this.modal.open();
    }
}