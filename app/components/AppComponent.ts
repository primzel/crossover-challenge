/**
 * Created by qasim on 8/31/16.
 */

import { Component } from '@angular/core';
/// <reference path="../../typings.d.ts" />

import * as io from 'socket.io-client';
import {MapComponent} from './map.component'

export class Hero {
    constructor(
        public id: number,
        public name: string,
        public power: string,
        public alterEgo?: string
    ) {  }
}
@Component({
    selector: 'my-app',
    templateUrl:"./templates/hero-form.component.html",
})
export class AppComponent {
    private socket:any
    constructor(){
        this.socket=io();
        this.socket.on('event', function(data:any){
            console.log(data);
        }.bind(this));
    }

    powers = ['Really Smart', 'Super Flexible',
        'Super Hot', 'Weather Changer'];
    model = new Hero(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet');
    submitted = false;
    onSubmit() { this.submitted = true; }
    // TODO: Remove this when we're done
    get diagnostic() { return JSON.stringify(this.model); }

}
