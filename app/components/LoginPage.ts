/**
 * Created by qasim on 9/1/16.
 */
import {Component} from "@angular/core";
// import {NgClass} from '@angular/common';
import {FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES} from '@angular/forms';
import {FormBuilder, FormGroup} from '@angular/forms';
;
import {CORE_DIRECTIVES, Validators} from '@angular/common';

@Component({
    selector: 'login-page',
    templateUrl: './templates/login-form.html',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES]

})
export class LoginPage {
    loginForm:FormGroup;

    constructor(fb:FormBuilder) {
        this.loginForm = fb.group({
            email: ["", Validators.required],
            password: ["", Validators.required],
        });
    }

    doLogin(event) {
        console.log(this.loginForm.value);
        event.preventDefault();
    }
}
