"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var map_service_1 = require('../services/map.service');
var geo_service_1 = require("../services/geo.service");
var MapView_1 = require("esri/views/MapView");
var Graphic_1 = require('esri/Graphic');
var PopupTemplate_1 = require('esri/PopupTemplate');
var Point_1 = require('esri/geometry/Point');
var SimpleMarkerSymbol_1 = require('esri/symbols/SimpleMarkerSymbol');
var form_modal_component_1 = require("../components/form.modal.component");
var io_service_1 = require("../services/io.service");
var MapComponent = (function () {
    function MapComponent(_service, _geoService, elRef, _ioService) {
        var _this = this;
        this._service = _service;
        this._geoService = _geoService;
        this.elRef = elRef;
        this._ioService = _ioService;
        this.viewCreated = new core_1.EventEmitter();
        this.addpin = function (data) {
            // Create a symbol for drawing the point
            var markerSymbol = new SimpleMarkerSymbol_1.default({
                color: [226, 119, 40],
                outline: {
                    color: [255, 255, 255],
                    width: 2
                }
            });
            console.log(data);
            var point = new Point_1.default({
                x: data.x,
                y: data.y,
                spatialReference: 2027
            });
            // Create a graphic and add the geometry and symbol to it
            var pointInfoTemplate = new PopupTemplate_1.default({
                title: 'Name : {name}',
                content: [{
                        type: "media",
                        mediaInfos: [{
                                title: "<b>{address},{contactNumber}</b>",
                                type: "image",
                                value: {
                                    sourceURL: "/images/blood.png" // use an attribute dynamically in the URL
                                }
                            }]
                    }]
            });
            var pointGraphic = new Graphic_1.default(point, markerSymbol, data, pointInfoTemplate);
            _this.view.graphics.add(pointGraphic);
        };
    }
    MapComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.view = new MapView_1.default({
            container: this.elRef.nativeElement.firstChild,
            map: this._service.map,
            zoom: 10,
            center: [31.5546, 74.3572]
        });
        this._ioService.on("addPin", this.addpin);
        this.view.on("click", function (evt) {
            if (evt.target.localName == "circle") {
                console.log(evt.target.localName);
            }
            else {
                _this.formModal.open(evt.mapPoint);
            }
            //this.view.graphics.removeAll();
            // I added this line to make sure the previous pin is removed if the new
            //feature is clicked. If there is a better way to handle it, let me know.
        });
        this.getCoordinates();
        this.viewCreated.next(this.view);
    };
    MapComponent.prototype.getCoordinates = function () {
        var _this = this;
        var cords;
        this._geoService.getPosition().subscribe(function (val) {
            val = val.loc.split(",");
            console.log(val);
            _this.view.center = new Point_1.default(val[1], val[0]);
        });
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], MapComponent.prototype, "viewCreated", void 0);
    __decorate([
        core_1.ViewChild("donerModal"), 
        __metadata('design:type', (typeof (_a = typeof form_modal_component_1.default !== 'undefined' && form_modal_component_1.default) === 'function' && _a) || Object)
    ], MapComponent.prototype, "formModal", void 0);
    MapComponent = __decorate([
        core_1.Component({
            selector: 'esri-map',
            template: '<div id="viewDiv"><ng-content></ng-content></div><form-modal #donerModal></form-modal>',
            providers: [map_service_1.default, geo_service_1.default, io_service_1.default],
        }), 
        __metadata('design:paramtypes', [map_service_1.default, geo_service_1.default, core_1.ElementRef, io_service_1.default])
    ], MapComponent);
    return MapComponent;
    var _a;
}());
exports.MapComponent = MapComponent;
