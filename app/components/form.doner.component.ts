/**
 * Created by qasimkhokhar on 9/5/16.
 */

import {FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES} from '@angular/forms';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';

import {CORE_DIRECTIVES, Validators} from '@angular/common';
import {Component, ViewChild, ElementRef, Input} from "@angular/core"
import {Doner} from "../Entities/doner.entity";
import IOService from "../services/io.service";
@Component({
    selector: "donner-form",
    templateUrl: "../templates/donner.form.template.html",
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES],
    providers: [IOService],
    inputs: ["dataobj"]
})
export class DonnerFormComponent {
    donnerForm:FormGroup;
    public dataobj:any;

    constructor(fb:FormBuilder, private _ioService:IOService) {
        this.donnerForm = new FormGroup({
            first_name: new FormControl("", Validators.required),
            last_name: new FormControl("", Validators.required),
            email: new FormControl("", Validators.required),
            contact_number: new FormControl("", Validators.required),
            bloodGroup: new FormControl("", Validators.required),
            address: new FormControl("", Validators.required),
        });
    }

    saveDonner(event) {
        console.log(this.donnerForm.value, this.dataobj);
        var payLoad = this.donnerForm.value;
        payLoad.x = this.dataobj.x;
        payLoad.y = this.dataobj.y;
        payLoad.mapPoint = this.dataobj;
        this._ioService.emit("saveDonner", this.donnerForm.value)
        event.preventDefault();
        return false;
    }
}