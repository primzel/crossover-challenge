
###How To Install Dependencies.
    1. npm install
    2. npm install body-parser
    2. npm install cookie-parser
    3. npm install shim
    4. npm install typescript
    5. npm install jade
    Note. You are required to install latest version of nodejs and MongoDb on you machine to deploy applciation.
###How To Build Project.
    1. npm run tsc
    2. npm start

###How To Test Application.
    1. You can access this application on `http://162.243.198.155:3000/`
    2. If you have deployed application on you localmachine use <you-machine-ip>:3000


