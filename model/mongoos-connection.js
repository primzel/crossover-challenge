/**
 * Created by qasim on 9/1/16.
 */
var config=require("../config");
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(config.connection_string);
module.exports=mongoose;
