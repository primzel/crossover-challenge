// grab the things we need
var mongoose=require("./mongoos-connection");
var Schema = mongoose.Schema;

// create a schema
var donnerSchema = new Schema({
    firstName: { type: String, required: true},
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    contactNumber: { type: String, required: true },
    bloodGroup: String,
    address: String,
    lat: Number,
    lng: Number,
    created_at: Date,
    updated_at: Date
});

// custom method to add string to end of name
// you can create more important methods like name validations or formatting
// you can also do queries and find similar users

// the schema is useless so far
// we need to create a model using it
var Donner = mongoose.model('Donner', donnerSchema);

// make this available to our users in our Node applications
module.exports = Donner;
